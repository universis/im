/* eslint-disable no-unused-vars */
import { TraceUtils } from '@themost/common';
import { DataObject, DataObjectState } from '@themost/data';
import { ServerSentEventService } from '@universis/sse'

/**
 * 
 * @param {import('@themost/data').DataEventArgs} event
 * @param {DataObjectState} state
 * @returns 
 */
async function emitSSE(event, state) {
  /**
   * get service
   * @type {ServerSentEventService}
   */
  const serverSentEventService = event.model.context.application.getService(ServerSentEventService);
  // if service has not been registered
  if (serverSentEventService == null) {
    // do nothing and exit
    return;
  }

  /**
   * create new context
   * @type {import('@themost/express').ExpressDataContext}
   */
  const context = event.model.context.application.createContext();

  try {
    /**
     * @type {DataObject}
     */
    const instantMessage = await context.model('InstantMessage')
      .where('id').equal(event.target.instantMessage)
      // .expand({ name: 'recipient' })
      .silent()
      .getTypedItem();

    if (!instantMessage) {
      return;
    }

    /**
     * @type {DataObject}
     */
    const messagingChannel = await context.model('MessagingChannel').convert(instantMessage.recipient);

    /**
    * get channnel members
    * @type {import('@themost/data').DataObjectJunction}
    */
    const channelMembers = messagingChannel.property('members');
    // exclude sender and get members
    const clients = [...serverSentEventService.clients];
    const members = await channelMembers.where('account').notEqual(event.target.sender).select('account/name as name').silent().getAllItems();

    members.forEach(member => {
      // send message to member
      const name = member.name.concat(':');
      const found = clients.filter(([key]) => key.startsWith(name));
      if (found.length) {
        for (const client of found) {
          const scope = client[1].scope;
          const user = client[1].user;

          // clone event.target without the property user
          // const entity = _.cloneDeep(_.omit(event.target, 'user'));
          const entity = (({ user, ...o }) => o)(event.target);

          serverSentEventService.emit(user, scope, {
            entitySet: 'InstantMessageReactions',
            entityType: 'InstantMessageReaction',
            entity: entity,
            messagingChannel: instantMessage.recipient,
            state: state
          });
        }
      }
    });
  } catch (err) {
    TraceUtils.error('An error occured while sending notification to channel members');
    TraceUtils.error(err);
  } finally {
    await context.finalizeAsync();
  }
}

/**
 * @param {import('@themost/data').DataEventArgs} event
 */
async function beforeRemoveAsync(event) {
  await emitSSE(event, DataObjectState.Delete);
}

/**
 * @param {import('@themost/data').DataEventArgs} event
 */
async function afterSaveAsync(event) {
  await emitSSE(event, DataObjectState.Insert);
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
  return afterSaveAsync(event).then(() => {
    return callback();
  }).catch((err) => {
    return callback(err);
  });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeRemove(event, callback) {
  return beforeRemoveAsync(event).then(() => {
    return callback();
  }).catch((err) => {
    return callback(err);
  });
}
