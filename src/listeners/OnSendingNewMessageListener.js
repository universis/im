/* eslint-disable no-unused-vars */
import { TraceUtils } from '@themost/common';
import { DataObject, DataObjectState } from '@themost/data';
import { ServerSentEventService } from '@universis/sse'
//
/**
 * @param {import('@themost/data').DataEventArgs} event
 */
async function afterSaveAsync(event) {
  if (event.state === DataObjectState.Insert) {
    /**
     * get service
     * @type {ServerSentEventService}
     */
    const serverSentEventService = event.model.context.application.getService(ServerSentEventService);
    // if service has not been registered
    if (serverSentEventService == null) {
      // do nothing and exit
      return;
    }
    /**
     * create new context
     * @type {import('@themost/express').ExpressDataContext}
     */
    const context = event.model.context.application.createContext();

    try {
      /**
       * @type {DataObject}
       */
      const messagingChannel = await context.model('MessagingChannel').where('id').equal(event.target.recipient).silent().getTypedItem();
      if (!messagingChannel) {
        return;
      }

      /**
    * get channnel members
    * @type {import('@themost/data').DataObjectJunction}
    */
      const channelMembers = messagingChannel.property('members');
      // exclude sender and get members
      const clients = [...serverSentEventService.clients];
      return channelMembers.where('id').notEqual(event.target.sender).select('account/name as name').silent().getAllItems().then((members) => {
        members.forEach(member => {
          serverSentEventService.emit(member.name, '*', {
            entitySet: 'InstantMessages',
            entityType: 'InstantMessage',
            entity: {
              identifier: event.target.identifier,
            }
          });
        });
      });
    } catch (err) {
      TraceUtils.error('An error occured while sending notification to channel members');
      TraceUtils.error(err);
    } finally {
      await context.finalizeAsync();
    }
  }
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
  return afterSaveAsync(event).then(() => {
    return callback();
  }).catch((err) => {
    return callback(err);
  });
}
