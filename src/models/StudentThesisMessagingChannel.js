import { HttpBadRequestError, HttpConflictError, HttpNotFoundError } from '@themost/common';
import { EdmMapping, EdmType } from '@themost/data';
import { MessagingChannel } from './MessagingChannel';

@EdmMapping.entityType('StudentThesisMessagingChannel')
class StudentThesisMessagingChannel extends MessagingChannel {
  /** @type {string=} */
  studentThesis;

  constructor() {
    super();
  }

  /**
   * This is an example and is NOT the final implementation.
   * @returns {import("@themost/data").DataQueryable}
   */
  @EdmMapping.param('notification', 'InstantNotification', false, true)
  @EdmMapping.action('Notify', 'InstantNotification')
  async notify(notification) {
    const channel = await this.getModel()
      .where('id').equal(this.getId())
      .expand({ name: 'maintainer', options: { $expand: 'account' } })
      .silent()
      .getItem();

    if (!channel) {
      throw new HttpConflictError('Messaging channel not found.');
    }

    // get thesis
    const studentThesis = await this.context.model('StudentThesis')
      .where('id').equal(channel.studentThesis)
      .expand({ name: 'thesis', options: { $expand: 'instructor($expand=user)' } })
      .silent()
      .getItem();

    if (studentThesis == null) {
      throw new HttpBadRequestError('Student thesis not found.');
    }

    const thesis = studentThesis.thesis;
    if (thesis == null) {
      throw new HttpBadRequestError('Thesis not found.');
    }

    if (thesis?.instructor?.user?.name !== this.context.user.name && channel.maintainer.account.name !== this.context.user.name) {
      throw new HttpBadRequestError('You are not allowed to send notifications to this channel.');
    }

    await this.context.model('InstantNotification').insert(Object.assign(notification, {
      recipient: this.id
    }));
    delete notification.$state;
    return notification;
  }

  @EdmMapping.action('PopulateMembers', EdmType.CollectionOf('InstantMessageAccount'))
  async populateMembers() {
    const channel = await this.getModel()
      .where('id').equal(this.getId())
      .expand({ name: 'maintainer', options: { $expand: 'account' } })
      .silent()
      .getItem();

    if (!channel) {
      throw new HttpNotFoundError('Messaging channel not found.');
    }

    // get thesis
    const studentThesis = await this.context.model('StudentThesis')
      .where('id').equal(channel.studentThesis)
      .expand(
        { name: 'thesis', options: { $expand: 'instructor($expand=user)' } },
        { name: 'student' }
      )
      .silent()
      .getItem();

    if (studentThesis == null) {
      throw new HttpNotFoundError('Student thesis not found.');
    }

    const thesis = studentThesis.thesis;
    if (thesis == null) {
      throw new HttpNotFoundError('Thesis not found.');
    }

    if (thesis?.instructor?.user?.name !== this.context.user.name && channel.maintainer.account.name !== this.context.user.name) {
      throw new HttpBadRequestError('You are not allowed to perform this action.');
    }

    const members = this.getMembers();
    const membersArr = await members.silent().getItems();

    if (studentThesis.student) {
      const student = await this.context.model('Student')
        .where('id').equal(studentThesis.student.id)
        .and('user').notEqual(null)
        .expand({ name: 'user', options: { $expand: { name: 'imAccount' } } })
        .silent()
        .getItem();

      (async () => {
        if (!student || !student.user || !student.user.imAccount) {
          // there is no IM account linked to this student
          return;
        }

        if (!membersArr.find((x) => x.id === student.user.imAccount.id)) {
          await members.insert(student.user.imAccount);
        }
      })()
    }

    if (studentThesis.thesis.instructor) {
      const instructor = await this.context.model('Instructor')
        .where('id').equal(studentThesis.thesis.instructor.id)
        .and('user').notEqual(null)
        .expand({ name: 'user', options: { $expand: { name: 'imAccount' } } })
        .silent()
        .getItem();


      (async () => {
        if (!instructor || !instructor.user || !instructor.user.imAccount) {
          // there is no IM account linked to this instructor
          return;
        }

        if (!membersArr.find((x) => x.id === instructor.user.imAccount.id)) {
          await members.insert(instructor.user.imAccount);
        }
      })();
    }

    return members;
  }

}

export {
  StudentThesisMessagingChannel
}