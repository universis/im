import { HttpBadRequestError, HttpConflictError } from '@themost/common';
import { EdmMapping } from '@themost/data';
import { MessagingChannel } from './MessagingChannel';

@EdmMapping.entityType('DirectMessagingChannel')
class DirectMessagingChannel extends MessagingChannel {
  constructor() {
    super();
  }

}

export {
  DirectMessagingChannel
}