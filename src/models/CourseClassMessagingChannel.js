import { HttpBadRequestError, HttpConflictError, HttpNotFoundError } from '@themost/common';
import { EdmMapping, EdmType } from '@themost/data';
import { MessagingChannel } from './MessagingChannel';

@EdmMapping.entityType('CourseClassMessagingChannel')
class CourseClassMessagingChannel extends MessagingChannel {
  /** @type {number=} */
  courseClass;

  constructor() {
    super();
  }

  /**
   * @returns {import("@themost/data").DataQueryable}
   */
  @EdmMapping.param('notification', 'InstantNotification', false, true)
  @EdmMapping.action('Notify', 'InstantNotification')
  async notify(notification) {
    const channel = await this.getModel()
      .where('id').equal(this.getId())
      .expand({ name: 'maintainer', options: { $expand: 'account' } })
      .silent()
      .getItem();

    if (!channel) {
      throw new HttpConflictError('Messaging channel not found.');
    }

    // get course class instructors
    const courseClass = await this.context.model('CourseClass')
      .where('id').equal(channel.courseClass)
      .expand({ name: 'instructors', options: { $expand: 'instructor($expand=user)' } })
      .silent()
      .getItem();

    if (courseClass == null) {
      throw new HttpBadRequestError('Course class not found.');
    }
    const instructors = courseClass.instructors;

    // check if current user is an instructor of this course class
    const currentInstructor = instructors.find((x) => x?.instructor?.user?.name === this.context.user.name);
    if (currentInstructor == null && channel.maintainer.account.name !== this.context.user.name) {
      throw new HttpBadRequestError('You are not allowed to send notifications to this channel.');
    }

    await this.context.model('InstantNotification').insert(Object.assign(notification, {
      recipient: this.id
    }));
    delete notification.$state;
    return notification;
  }

  /**
   * @returns {import("@themost/data").DataObjectJunction}
   */
  @EdmMapping.action('PopulateMembers', EdmType.CollectionOf('InstantMessageAccount'))
  async populateMembers() {
    const channel = await this.getModel()
      .where('id').equal(this.getId())
      .expand({ name: 'maintainer', options: { $expand: 'account' } })
      .silent()
      .getItem();

    if (!channel) {
      throw new HttpNotFoundError('Messaging channel not found.');
    }

    // get course class with instructors and students
    const courseClass = await this.context.model('CourseClass')
      .where('id').equal(channel.courseClass)
      .expand(
        { name: 'instructors', options: { $expand: 'instructor($expand=user)' } },
        { name: 'students' }
      )
      .silent()
      .getItem();

    if (courseClass == null) {
      throw new HttpNotFoundError('Course class not found.');
    }

    const instructors = courseClass.instructors;
    const students = courseClass.students;

    // check if current user is an instructor of this course class
    const currentInstructor = instructors.find((x) => x?.instructor?.user?.name === this.context.user.name);
    if (currentInstructor == null && channel.maintainer.account.name !== this.context.user.name) {
      throw new HttpBadRequestError('You are not allowed to perform this action.');
    }

    const members = this.getMembers();
    const membersArr = await members.silent().getItems();

    const doPopulate = async (modelName, expandModel, items) => {
      if (!items) return;

      for (const x of items) {
        const item = await this.context.model(modelName)
          .where('id').equal(x[expandModel])
          .and('user').notEqual(null)
          .expand({ name: 'user', options: { $expand: { name: 'imAccount' } } })
          .silent()
          .getItem();

        if (!item || !item.user || !item.user.imAccount) {
          continue;
        }

        await new Promise((resolve, reject) => {
          void this.context.db.executeInTransaction((cb) => {
            (async () => {
              await members.insert(membersArr.filter((i) => {
                return i.id !== item.user.imAccount.id;
              }));
            })().then(() => {
              return cb();
            }).catch((err) => {
              return cb(err);
            })
          }, (err) => {
            if (err) return reject(err);
            return resolve();
          });
        });

      }
    }

    await doPopulate('Student', 'student', students);
    await doPopulate('Instructor', 'instructor', instructors);

    return members;
  }

}

export {
  CourseClassMessagingChannel
}