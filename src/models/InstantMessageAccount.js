import { HttpNotFoundError } from '@themost/common';
import { DataObject, EdmMapping } from '@themost/data';

@EdmMapping.entityType('InstantMessageAccount')
class InstantMessageAccount extends DataObject {
  /** @type {number=} */
  id;
  /** @type {string=} */
  name;
  /** @type {number=} */
  account;

  constructor() {
    super();
  }

  @EdmMapping.func('user', 'User')
  async getUser() {
    const user = await this.context.model('User')
      .where('imAccount/id').equal(this.getId())
      .expand('imAccount', 'groups')
      .silent()
      .getItem();

    if (!user) {
      throw new HttpNotFoundError('User not found.');
    }

    return user;
  }

  /**
   * Try to find the student data object associated with this IM account
   * @returns {import("@themost/data").DataQueryable}
   */
  @EdmMapping.func('student', 'Student')
  async getStudent() {
    const student = await this.context.model('Student')
      .where('user/imAccount/id').equal(this.getId())
      .silent()
      .getItem();

    if (!student) {
      throw new HttpNotFoundError('Student not found.');
    }

    return student;
  }

  /**
   * Try to find the instructor data object associated with this IM account
   * @returns {import("@themost/data").DataQueryable}
   */
  @EdmMapping.func('instructor', 'Instructor')
  async getInstructor() {
    const instructor = await this.context.model('Instructor')
      .where('user/imAccount/id').equal(this.getId())
      .silent()
      .getItem();

    if (!instructor) {
      throw new HttpNotFoundError('Instructor not found.');
    }

    return instructor;
  }
}

export {
  InstantMessageAccount
}