import { ApplicationService } from '@themost/common';
import { SchemaLoaderStrategy } from '@themost/data';
import path from 'path';

class AccountReplacer extends ApplicationService {
  constructor(app) {
    super(app);
  }

  apply() {
    // get schema loader
    const schemaLoader = this.getApplication().getConfiguration().getStrategy(SchemaLoaderStrategy);
    // get model definition
    const model = schemaLoader.getModelDefinition('Account');
    const findAttribute = model.fields.find((field) => {
      return field.name === 'imAccount'
    });
    if (findAttribute == null) {
      model.fields.push({
        "name": "imAccount",
        "type": "InstantMessageAccount",
        "many": true,
        "nested": true,
        "multiplicity": "ZeroOrOne",
        "mapping": {
          "associationType": "association",
          "cascade": "delete",
          "parentModel": "Account",
          "parentField": "id",
          "childModel": "InstantMessageAccount",
          "childField": "account",
        }
      });
      model.eventListeners = model.eventListeners || [];
      model.eventListeners.push({
        type: path.resolve(__dirname, 'listeners/OnBeforeJoinInstantMessageAccount')
      });
      schemaLoader.setModelDefinition(model);
    }
  }

}

export {
  AccountReplacer
}
