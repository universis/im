import { ApplicationService } from "@themost/common";
import LocalScopeAccessConfiguration from './config/scope.access.json';
import { AccountReplacer } from "./AccountReplacer";
export class MessagingService extends ApplicationService {
  constructor(app) {
    super(app);

    new AccountReplacer(app).apply();

    // extend universis api scope access configuration
    if (app && app.container) {
      app.container.subscribe((container) => {
        if (container) {
          // add extra scope access elements
          const scopeAccess = app.getConfiguration().getStrategy(function ScopeAccessConfiguration() { });
          if (scopeAccess != null) {
            scopeAccess.elements.push.apply(scopeAccess.elements, LocalScopeAccessConfiguration);
          }
        }
      });
    }
  }

}